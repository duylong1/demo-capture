// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  public_key: '9eWzpGj7AakEf8yQbq8ccrWYvdXyXK',
  base_url: 'https://cs-dev.etelecom.vn',
  sip_server: 'wss://sip-dev.dinodata.vn:10443',
  firebase: {
    apiKey: 'AIzaSyAuiks09J7twzd8lWEVERW6m0AFMGH2Zdc',
    authDomain: 'fir-capture-93a1e.firebaseapp.com',
    projectId: 'fir-capture-93a1e',
    storageBucket: 'fir-capture-93a1e.appspot.com',
    messagingSenderId: '730516474668',
    appId: '1:730516474668:web:78441fa1654d7f80b67a89',
    measurementId: 'G-XYGB6NZ3SG',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
