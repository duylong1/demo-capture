import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
  } from "@angular/common/http";
  import { BehaviorSubject, Observable, throwError } from "rxjs";
  import {
    catchError,
  } from "rxjs/operators";
  import { Injectable } from "@angular/core";
import { AuthService } from 'src/lib/api/auth.service';

  @Injectable()
  export class MyHttpInterceptor implements HttpInterceptor {
    constructor(private auth : AuthService) {}
    intercept(
      request: HttpRequest<any>,
      next: HttpHandler
    ): Observable<HttpEvent<any>> {
      const token = this.auth.activeUser?.access_token;
      if (token) {
        request = request.clone({
          setHeaders: {
            authorization: "Bearer " + token,
            "X-Request-Client": "9eWzpGj7AakEf8yQbq8ccrWYvdXyXK",
          },
        });
      }

      return next.handle(request).pipe(
        catchError((err) => {
          return throwError(err);
        })
      );
    }
  }
