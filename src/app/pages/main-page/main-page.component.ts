import {
  MediaDevicesCheckComponent,
  facingMode,
} from './../../component/media-devices-check/media-devices-check.component';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/lib/api/auth.service';
import {
  Invitation,
  Inviter,
  InviterInviteOptions,
  Registerer,
  SessionState,
  UserAgent,
  Web,
} from 'sip.js';
import { ExtensionService } from 'src/lib/api/extension.service';
import { environment } from 'src/environments/environment';
import { IncomingResponse } from 'sip.js/lib/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
})
export class MainPageComponent implements OnInit {
  @ViewChild('remoteVideo') declare remoteVideoEl: ElementRef;
  declare localVideoStream: MediaStream;
  declare remoteVideoStream: any;

  declare callInProgress: boolean;
  declare remoteVideoReady: boolean;
  declare callOnMaking: boolean;
  declare mediaDeviceReady: boolean;
  declare sipInitialized: boolean;

  declare constrainOption: any;

  declare extension: any;
  declare session: any;
  declare outGoinginviter: Inviter;
  declare userAgent: UserAgent;
  declare registerer: Registerer;
  declare incomingInvitation: Invitation;

  declare isMuteLocalAudio: boolean;
  declare isTurnOffLocalVideo: boolean;

  constructor(
    public dialog: MatDialog,
    private authService: AuthService,
    private extService: ExtensionService,
    private router: Router
  ) {}

  ngOnInit() {
    this.callInProgress = false;
    this.callOnMaking = false;
    this.openMediaDevicesCheckDialog();
  }

  ext: any = '7207';
  setCallNumber() {
    const ext = prompt('call to ?');
    this.ext = ext ?? '7207';
  }

  openMediaDevicesCheckDialog(): void {
    const dialogRef = this.dialog.open(MediaDevicesCheckComponent, {
      disableClose: true,
      width: '95%',
      height: 'auto',
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.localVideoStream = result.stream;
      this.constrainOption = result.constrain;
      this.isTurnOffLocalVideo = result.isTurnOffLocalVideo;
      this.isMuteLocalAudio = result.isMuteLocalAudio;
      this.mediaDeviceReady = true;
      setTimeout(async () => {
        await this.initExtension();
      });
    });
  }

  async initExtension() {
    await this.authService.login({
      login: '0584109313',
      password: '0584109313',
    });

    this.authService.$isLogin.subscribe((res) => {
      if (res) {
        this.registerPortsip();
      }
    });
  }

  registerPortsip() {
    this.extService.getExtensions().subscribe(async (res) => {
      this.extension = res;

      const userExtension = this.currentExtension(
        this.authService.activeUser.user.id
      );
      const { extension_number, extension_password, tenant_domain } =
        userExtension;

      const _uri = UserAgent.makeURI(
        `sip:${extension_number}@${tenant_domain}`
      );

      if (!_uri) {
        return;
      }

      const _userAgentOptions: any = {
        uri: _uri,
        register: true,
        authorizationUsername: extension_number,
        authorizationPassword: 'DsaCW4JLVA',
        transportOptions: {
          server: environment.sip_server,
        },
        logLevel: 'error',
        displayName: extension_number,
        delegate: {
          onInvite: (invitation: Invitation) => {
            this.receiveCall(invitation);
          },
        },
      };

      this.userAgent = new UserAgent(_userAgentOptions);

      this.registerer = new Registerer(this.userAgent, {});

      await this.userAgent.start().catch((err) => {
        console.log(err);
      });

      await this.registerer
        .register({
          requestDelegate: {
            onAccept: (_) => {},
            onReject: (resp) => {
              if (resp.message.statusCode) {
              }
            },
          },
        })
        .catch((err) => {
          console.log(err);
        });

      this.sipInitialized = true;
      setTimeout(() => {
        this.makeCall(this.ext);
      }, 3000);
    });
  }

  async receiveCall(invitation: Invitation) {
    this.incomingInvitation = invitation;
  }

  async makeCall(callTarget: string) {
    this.callOnMaking = true;
    setTimeout(() => {
      const ringBackAudio: any = document.getElementById('ringBackAudio');
      if (ringBackAudio) {
        ringBackAudio.play();
      }
    });
    const userExtension = this.currentExtension(
      this.authService.activeUser.user.id
    );
    const { tenant_domain } = userExtension;
    const target = UserAgent.makeURI(`sip:${callTarget}@${tenant_domain}`);
    if (!target) {
      return;
    }
    try {
      const constraints = this.constrainOption;

      const inviter = new Inviter(this.userAgent, target, {
        // extraHeaders: [...this.extraHeader],
      });
      this.outGoinginviter = inviter;
      this.outGoinginviter.stateChange.addListener((state: SessionState) => {
        switch (state) {
          case SessionState.Terminated:
            this.endCalls();
            break;
          default:
            break;
        }
      });
      const _inviterInviteOptions: InviterInviteOptions = {
        sessionDescriptionHandlerOptions: {
          constraints,
        },
        requestDelegate: {
          // Khi cuộc gọi bắt đầu
          onProgress: (response: IncomingResponse) => {},
          // Khi cuộc gọi được người nhận chấp nhận
          onAccept: (response: IncomingResponse) => {
            this.session = inviter;
            this._processCalls();
          },
        },
      };
      await this.outGoinginviter.invite(_inviterInviteOptions);
    } catch (e) {
      console.error('ERROR in answer', e);
    }
  }

  sessionStateListener() {
    this.session.stateChange.addListener((state: SessionState) => {
      switch (state) {
        case SessionState.Terminated:
          this.endCalls();
          break;
        default:
          break;
      }
    });
  }

  async hangup() {
    if (
      this.session.state == SessionState.Initial ||
      this.session.state == SessionState.Establishing
    ) {
      await this.outGoinginviter.cancel();
    } else {
      await this.session?.bye();
    }
    this.endCalls();
  }

  facingUser : boolean = true;

  async switchCamera() {
    const facingMode = this.facingUser?'environment':'user';
    this.facingUser = !this.facingUser
    this.constrainOption = {
      audio: true,
      video: {
        width: 1280,
        height: 720,
        facingMode: facingMode
      },
    };

    console.log(this.constrainOption.facingMode);

    try {
      if (this.localVideoStream) {
        const tracks = this.localVideoStream.getTracks();
        tracks.forEach((track) => track.stop());
      }
      let stream = await navigator.mediaDevices.getUserMedia(
        this.constrainOption
      );
      if (stream) {
        this.localVideoStream = stream;
        const pc = this.session!.sessionDescriptionHandler!.peerConnection;
        pc.getSenders().forEach((sender: RTCRtpSender) => {
          const replacingTrack = stream
            .getTracks()
            .find((s) => s.kind == sender.track?.kind);
          if (replacingTrack) {
            sender.replaceTrack(replacingTrack);
          }
        });
      }
    } catch (error) {
      // throw err
    }
  }

  async endCalls() {
    this.remoteVideoStream = new MediaStream();
    this.localVideoStream.getTracks().forEach(function (track: any) {
      track.stop();
    });
    this.localVideoStream = new MediaStream();
    // this.router.navigate(['call-ended']);
  }

  private _processCalls() {
    const sessionDescriptionHandler = this.session.sessionDescriptionHandler;
    if (
      sessionDescriptionHandler &&
      sessionDescriptionHandler instanceof Web.SessionDescriptionHandler
    ) {
      const srcObject = sessionDescriptionHandler.remoteMediaStream;

      this.processCallAudio();
      this.processCallVideo();
      this.remoteVideoReady = true;
      this.callInProgress = true;
      setTimeout(() => {
        this.remoteVideoStream = srcObject;
        if (this.remoteVideoEl.nativeElement) {
          this.remoteVideoEl.nativeElement.playsInline = true;
          this.remoteVideoEl.nativeElement.srcObject = srcObject;
        }
      });
      this.callOnMaking = false;
    }
    const ringBackAudio: any = document.getElementById('ringBackAudio');
    if (ringBackAudio) {
      ringBackAudio.pause();
      ringBackAudio.currentTime = 0;
    }
    this.sessionStateListener();
  }

  currentExtension(id: any) {
    const extensions: any[] = this.extension.extensions;
    return extensions.find((ext) => ext.user_id == id);
  }

  processCallAudio() {
    if (!this.localVideoStream.getAudioTracks()[0]) {
      return;
    }
    this.localVideoStream.getAudioTracks()[0].enabled = !this.isMuteLocalAudio;
    var pc = this.session!.sessionDescriptionHandler!.peerConnection;
    if (!pc) {
      return;
    }
    const state = this;
    console.log(pc);

    pc.getSenders().forEach(function (sender: any) {
      if (sender.track.kind == 'audio') {
        sender.track.enabled = !state.isMuteLocalAudio;
      }
    });
  }

  processCallVideo() {
    setTimeout(() => {
      const state = this;
      this.localVideoStream.getTracks().forEach(function (track: any) {
        if (track.kind == 'video') {
          track.enabled = !state.isTurnOffLocalVideo;
        }
      });

      var pc = this.session!.sessionDescriptionHandler!.peerConnection;
      if (!pc) {
        return;
      }

      pc.getSenders().forEach(function (sender: any) {
        if (sender.track.kind == 'video') {
          sender.track.enabled = !state.isTurnOffLocalVideo;
        }
      });
    });
  }
}
