import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  Invitation,
  Inviter,
  InviterInviteOptions,
  Registerer,
  Session,
  SessionState,
  UserAgent,
  UserAgentOptions,
  Web,
} from 'sip.js';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ExtensionService } from './extension.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  activeUser!: any;
  $isLogin : BehaviorSubject<boolean> = new BehaviorSubject(false)

  constructor(private http: HttpClient, private extSerivce: ExtensionService) {}

  async login(data: any) {
    await this.http
      .post(`${environment.base_url}/api/etop.User/Login`, {
        ...data,
        account_type: 'shop',
      })
      .subscribe(async (val) => {
        this.activeUser = val;
        this.$isLogin.next(true)
      });
  }



}
