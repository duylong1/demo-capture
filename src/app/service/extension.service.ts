import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ExtensionService {
  $isPortsipRegistered: BehaviorSubject<any> = new BehaviorSubject(false);
  get isPortsipRegistered(){
    return this.$isPortsipRegistered.getValue()
  }
  constructor(private http: HttpClient) {}

  getExtensions(request?: ExtensionAPI.GetContactsRequest) {
    return this.http
      .post(
        `${environment.base_url}/api/shop.Etelecom/GetExtensions`,
        request || {}
      );
  }
}

export namespace ExtensionAPI {
  export interface GetContactsRequest {
    filter: GetExtensionsFilter;
  }

  export interface GetExtensionsFilter {
    extension_numbers?: string[];
    hotline_id?: string;
  }
}
