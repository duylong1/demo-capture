import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
export const facingMode = ['user', 'environment'];

@Component({
  selector: 'app-media-devices-check',
  templateUrl: './media-devices-check.component.html',
  styleUrls: ['./media-devices-check.component.scss'],
})
export class MediaDevicesCheckComponent implements OnInit {
  declare localVideoStream: any;
  private _isPermissionGranted!: boolean;
  public get isPermissionGranted(): boolean {
    return this._isPermissionGranted;
  }
  public set isPermissionGranted(value: boolean) {
    this._isPermissionGranted = value;
  }

  declare videoStream: any;
  declare selectedCam: boolean;
  declare constrainOption: any;

  declare isMuteLocalAudio: boolean;
  declare isTurnOffLocalVideo: boolean;

  constructor(public dialogRef: MatDialogRef<MediaDevicesCheckComponent>) {}

  ngOnInit() {
    this.setupMediaDevice();
    this.isMuteLocalAudio = false;
    this.isTurnOffLocalVideo = false;
  }

  async setupMediaDevice() {
    await this.setupCameraDevice(true);
  }

  async setupCameraDevice(selectedCam: boolean) {
    this.selectedCam = selectedCam;
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      try {
        const supports = navigator.mediaDevices.getSupportedConstraints();
        this.constrainOption = {
          audio: true,
          video: {
            width: 1280,
            height: 720,
            facingMode: selectedCam ? facingMode[0] : facingMode[1],
          },
        };

        try {
          if (this.localVideoStream) {
            const tracks = this.localVideoStream.getTracks();
            tracks.forEach((track: any) => track.stop());
          }

          let stream = await navigator.mediaDevices.getUserMedia(
            this.constrainOption
          );

          this.isPermissionGranted = true;
          this.localVideoStream = stream;
        } catch (e) {
          alert(e);
          return;
        }
      } catch (e) {
        this.isPermissionGranted = false;
      }
    } else {
      this.isPermissionGranted = false;
    }
  }

  switchCamera() {
    this.setupCameraDevice(!this.selectedCam);
  }

  stopVideo(event: any) {
    this.isTurnOffLocalVideo = !event.checked;
    this.localVideoStream.getTracks().forEach(function (track: any) {
      if (track.kind == 'video') {
        track.enabled = event.checked;
      }
    });
  }

  muteAudio(event: any) {
    this.isMuteLocalAudio = !event.checked;
    this.localVideoStream.getAudioTracks()[0].enabled = event.checked;
  }

  submit() {
    this.dialogRef.close({
      stream: this.localVideoStream,
      constrain: this.constrainOption,
      isTurnOffLocalVideo : this.isTurnOffLocalVideo,
      isMuteLocalAudio : this.isMuteLocalAudio,
    });
  }
}
