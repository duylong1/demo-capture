import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CallEndedComponent } from './pages/call-ended/call-ended.component';
import { MainPageComponent } from './pages/main-page/main-page.component';


const routes: Routes = [
  {path : 'main', component : MainPageComponent},
  {path : 'call-ended', component : CallEndedComponent},
  {path : '**', pathMatch : 'full', redirectTo : 'main'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
